import cv2

videoCam = cv2.VideoCapture(0)

face = cv2.CascadeClassifier('data/haarcascades/face-detect.xml')
eye = cv2.CascadeClassifier('data/haarcascades/eye-detect.xml')
smile = cv2.CascadeClassifier('data/haarcascades/smile-detect.xml')

while True:
    cond, frame = videoCam.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    muka = face.detectMultiScale(gray, 1.3, 5)
    for (x,y,w,h) in muka:
        cv2.rectangle(frame, (x,y), (x+w, y+h), (0, 255, 0), 5)
        


        roi_warna = frame[y:y+h, x:x+w]
        roi_gray = gray[y:y+h, x:x+w]
        mata = eye.detectMultiScale(roi_gray)
        for (mx,my,mw,mh)in mata:
            cv2.rectangle(roi_warna, (mx,my), (mx+mw, my+mh), (255,255,0), 2)

        senyum = smile.detectMultiScale(roi_gray, 1.8, 20)
        for (sx, sy, sw, sh) in senyum:
            cv2.rectangle(roi_warna, (sx, sy), ((sx + sw), (sy + sh)), (0, 0, 255), 2)
    
    cv2.imshow('Face dan Eye detection', frame)

    k = cv2.waitKey(1) & 0xff
    if k == ord('q'):
        break

videoCam.release()
cv2.destroyAllWindows()